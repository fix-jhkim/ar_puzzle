﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Test : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

   public GameObject test_obj;

   private int step;

    // Use this for initialization
    void Start () {
     //   step = 0;
    }
	
	// Update is called once per frame
	void Update () {
       
    }

    public void BtnRotation()
    {
        //step++;
        //if (step > 4)
        //    step = 1;

        //test_obj.transform.eulerAngles = new Vector3(90 * step, 0, 0);

        //if (isBtnDown)
        //{
        //    test_obj.transform.Rotate(Vector3.forward * 10.0f);
        //}
        ARSound.instance.PlaySound();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.tag);
    }
    
    public void Left()
    {
        test_obj.transform.Rotate(Vector3.forward * 10.0f);
    }

    public void Right()
    {
        test_obj.transform.Rotate(Vector3.back * 10.0f);
    }

    private bool isBtnDown = false;

    public void OnPointerDown(PointerEventData eventData)
    {
        isBtnDown = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isBtnDown = false;
    }
}
