﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompareObjects : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == gameObject.tag)
        {
            other.GetComponent<MeshRenderer>().material.color = Color.white;
            Singleton.Instance().TouchState = "Null";
            Singleton.Instance().NowPieces++;
            Singleton.Instance().NowDivision++;
            Singleton.Instance().CrashTag = gameObject.tag;

            

            if (Singleton.Instance().TotalDivision == Singleton.Instance().NowDivision)
            {
                Singleton.Instance().NowDivision = 0;
                Singleton.Instance().NowPieceStep++;
                GameObject.Find("Main Controller").GetComponent<ARController>().PieceGeneration(Singleton.Instance().NowPieceStep);

                Debug.Log("OnTriggerEnter_ NowPieceStep : " + Singleton.Instance().NowPieceStep);
            }
            Destroy(gameObject);
        }
    }
}