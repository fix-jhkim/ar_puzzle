﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceGetDrop : MonoBehaviour {

    private bool B_mDrop;
    private float F_mspeed;
    //private Vector3 V3_mTarget;

    private Vector3 V3_mTarget_Origin;

    void Start () {
        B_mDrop = false;
        F_mspeed = 1.0f;
       // V3_mTarget = new Vector3(gameObject.transform.position.x, 0, gameObject.transform.position.z);
    }

    public void PostionMemory()
    {
        V3_mTarget_Origin = gameObject.transform.position;
    }

    void Update ()
    {        
        if (B_mDrop)
        {
            float step = F_mspeed * Time.deltaTime;
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, V3_mTarget_Origin, step);
        }

        if (V3_mTarget_Origin == gameObject.transform.position && B_mDrop == true) {
            B_mDrop = false;
        }
    }

    public void ObjectDrop()
    {
        B_mDrop = true;
    }
}