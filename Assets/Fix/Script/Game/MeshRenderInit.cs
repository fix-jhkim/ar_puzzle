﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshRenderInit : MonoBehaviour {

    public Material Glass;
    public string Type;

    private int Divisions;

    // Use this for initialization
    void Start() {
        if (Type == "Object")
        {
            SetTag();
        }
        else if (Singleton.Instance().NowPieceStep == 1)
        {
            SetTag();
        }
    }

    public void SetTag()
    {
        List<Dictionary<string, object>> data = CSVReader.Read("PieceLevelDesign_" + Singleton.Instance().GameNum);

        Divisions = 0;
        if (Singleton.Instance().NowPieceStep >= 2)
        {
            for (int iNum = 0; iNum < Singleton.Instance().NowPieceStep - 1; iNum++)
            {
                Divisions = Divisions + int.Parse(data[iNum]["Divisions"].ToString());
            }
        }

        for (int iNum = 0; iNum < gameObject.transform.childCount; iNum++)
        {
            //gameObject.transform.GetChild(iNum).GetComponent<MeshRenderer>().material.color = Color.white;
            if (Type == "Piece")
            {                
                if (Singleton.Instance().NowPieceStep >= 2)
                {
                    gameObject.transform.GetChild(iNum).tag = (iNum + Divisions + 1).ToString();
                }
                else
                {
                    gameObject.transform.GetChild(iNum).tag = (iNum + 1).ToString();
                }
                gameObject.transform.GetChild(iNum).GetComponent<MeshRenderer>().material.color = Color.white;
            }

            if (Type == "Object")
            {
                gameObject.transform.GetChild(iNum).tag = (iNum + 1).ToString();
                gameObject.transform.GetChild(iNum).GetComponent<MeshRenderer>().material.color = new Color32(0, 156, 255, 1);
                //gameObject.transform.GetChild(iNum).GetComponent<Renderer>().material = Glass;
            }
        }
    }

    public void PieceMeshInit()
    {
        for (int iNum = 0; iNum < gameObject.transform.childCount; iNum++)
        {
            if (Type == "Piece")
            {
                gameObject.transform.GetChild(iNum).GetComponent<MeshRenderer>().material.color = Color.white;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Complete()
    {
        for (int iNum = 0; iNum < gameObject.transform.childCount; iNum++)
        {
            gameObject.transform.GetChild(iNum).GetComponent<Renderer>().material = Glass;           
        }
    }
}
