﻿using System.Collections.Generic;
using GoogleARCore;
using GoogleARCore.Examples.Common;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
// Set up touch input propagation while using Instant Preview in the editor.
using Input = GoogleARCore.InstantPreviewInput;
#endif

/// <summary>
/// ARController 2018-06-18 김재환
/// </summary>
public class ARController : MonoBehaviour
{
    /// <summary>
    /// 1인칭 카메라는 AR배경을 랜더링하는데 사용된다. // 2018-06-18 김재환
    /// The first-person camera being used to render the passthrough camera image (i.e. AR background).
    /// </summary>
    public Camera FirstPersonCamera;

    /// <summary>
    /// 탐색 된 평면 추적 및 시각화를 위한 프리팹 // 2018-06-18 김재환
    /// A prefab for tracking and visualizing detected planes.
    /// </summary>
    public GameObject DetectedPlanePrefab;

    /// <summary>
    /// 사용될 오브젝트 모델 프리팹 // 2018-06-18 김재환
    /// A model to place when a raycast from a user touch hits a plane.
    /// </summary>
    public GameObject AndyAndroidPrefab;

    /// <summary>
    /// 사용될 오브젝트 모델 프리팹 // 2018-07-05 김재환
    /// A model to place when a raycast from a user touch hits a plane.
    /// </summary>
    public GameObject PicesPrefab;

    /// <summary>
    /// 바닥 오브젝트 모델 프리팹 // 2018-07-05 김재환
    /// A model to place when a raycast from a user touch hits a plane.
    /// </summary>
    public GameObject BottomPrefab;

    /// <summary>
    /// 하단 UI 평면을 찾는중 디스플레이 // 2018-06-18 김재환
    /// A gameobject parenting UI for displaying the "searching for planes" snackbar.
    /// </summary>
    public GameObject SearchingForPlaneUI;

    /// <summary>
    /// Andy 모델을 배치할때 각도고려값 // 2018-06-18 김재환
    /// The rotation in degrees need to apply to model when the Andy model is placed.
    /// </summary>
    private const float k_ModelRotation = 90.0f;

    /// <summary>
    /// ARCore가 현재 프레임에서 추적중인 모든 평면을 유지하는 목록이다. // 2018-06-18 김재환
    /// 이 개체는 응용프로그램에서 최적화를 위해 사용된다.
    /// A list to hold all planes ARCore is tracking in the current frame. This object is used across
    /// the application to avoid per-frame allocations.
    /// </summary>
    private List<DetectedPlane> m_AllPlanes = new List<DetectedPlane>();

    /// <summary>
    /// 응용프로그램이 ARcore 연결 오류로 인해 종료되는 경우는 true 이고, 그렇지 않으면 false이다. // 2018-06-18 김재환
    /// True if the app is in the process of quitting due to an ARCore connection error, otherwise false.
    /// </summary>
    private bool m_IsQuitting = false;


    /// <summary>
    /// 테스트를 위한 함수 // 2018-06-19 김재환
    /// Functions for testing
    /// </summary>
    private bool m_IsGnrt = true;
    private bool m_IsZoom = true;
    //private Ray ray;
    private GameObject m_GOTest;
    private GameObject m_GOPiece;
    private GameObject m_GOBottom;

    /// <summary>
    /// UI 컨트롤을위한 선언 // 2018-07-12 김재환
    /// </summary>
    public GameObject GameObject_Cross;
    public GameObject GameObject_rock_postion;
    public GameObject GameObject_scalider_zoom;
    public GameObject GameObject_rock_scale;
    public GameObject GameObject_getdrop;
    public GameObject GameObject_Slider_Level;
    public GameObject GameObject_piece_arrangement;
    public GameObject GameObject_Result;
    public GameObject GameObject_Scrren;
    public GameObject GameObject_Shop;
    public GameObject GameObject_Shop_popup;
    public GameObject GameObject_Back_popup;

    /// <summary>
    /// 조각 크기 저장
    /// 다음단계 조각의 크기를 결정한다. // 2018-08-07 김재환
    /// </summary>
    private float m_fPieceScale;


    /// <summary>
    /// PieceLevelDesign_ 엑셀파일 연동을 위한 선언 // 2018-07-24 김재환
    /// </summary>
    List<Dictionary<string, object>> data;

    /// <summary>
    /// 오브젝트가 생성된 위치값 기억하기 // 2018-07-25 김재환
    /// </summary>
    public TrackableHit TH_OriginalLocation;

    /// <summary>
    /// 유니티 시작 함수 // 2018-07-12 김재환
    /// </summary>
    private void Start()
    {
        data = CSVReader.Read("PieceLevelDesign_"+ Singleton.Instance().GameNum);
        Singleton.Instance().TotalPieceStep = int.Parse(data[0]["PieceStep"].ToString());
        Singleton.Instance().NowPieceStep = 1;

        Singleton.Instance().TotalDivision = int.Parse(data[0]["Divisions"].ToString());
        Singleton.Instance().NowDivision = 0;

        Singleton.Instance().GameState = "GameReady";
        Singleton.Instance().TouchState = "Null";
        Singleton.Instance().TotalPieces = int.Parse(data[0]["TotalPieces"].ToString());
        Singleton.Instance().CrashTag = "";

        GameObject_Slider_Level.transform.GetComponent<Slider>().maxValue = Singleton.Instance().TotalPieces;
        Singleton.Instance().NowPieces = 0;
        GameObject_rock_scale.SetActive(false);
        GameObject_scalider_zoom.SetActive(false);
        GameObject_piece_arrangement.SetActive(false);       
    }

    /// <summary>
    /// 유니티 업데이트 함수 // 2018-06-18 김재환
    /// The Unity Update() method.
    /// </summary>
    public void Update()
    {
        _UpdateApplicationLifecycle();

        // 하나 이상의 평면을 찾는다면 하단의 UI바를 숨긴다.  // 2018-06-18 김재환
        // Hide snackbar when currently tracking at least one plane.
        if (m_IsZoom)
        {
            Session.GetTrackables<DetectedPlane>(m_AllPlanes);
            bool showSearchingUI = true;
            for (int i = 0; i < m_AllPlanes.Count; i++)
            {
                if (m_AllPlanes[i].TrackingState == TrackingState.Tracking)
                {
                    showSearchingUI = false;
                    break;
                }
            }
            SearchingForPlaneUI.SetActive(showSearchingUI);
        }

        // 오브젝트 회전 // 2018-07-18 김재환
        if (Singleton.Instance().GameState == "GameStart" || Singleton.Instance().GameState == "GameEnd")
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Moved)
                {
                    m_GOTest.transform.Rotate(0, 0, -touch.deltaPosition.x * 0.5f);
                }
            }
        }
    }

    /// <summary>
    /// 각 상황에 대하여 판단 // 2018-06-18 김재환
    /// Check and update the application lifecycle.
    /// </summary>
    private void _UpdateApplicationLifecycle()
    {
        // 앱에서 back 버튼 눌렀을때 // 2018-06-18 김재환
        // Exit the app when the 'back' button is pressed.
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        // 추적하지 않을때 잠금상태로 전환 // 2018-06-18 김재환
        // Only allow the screen to sleep when not tracking.
        if (Session.Status != SessionStatus.Tracking)
        {
            const int lostTrackingSleepTimeout = 15;
            Screen.sleepTimeout = lostTrackingSleepTimeout;
        }
        else
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        if (m_IsQuitting)
        {
            return;
        }

        // ARCore가 연결되지 않았고 유니티에 메시지가 나타날 때까지 기다렸다가 종료하기 // 2018-06-18 김재환
        // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
        if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
        {
            _ShowAndroidToastMessage("Camera permission is needed to run this application.");
            m_IsQuitting = true;
            Invoke("_DoQuit", 0.5f);
        }
        else if (Session.Status.IsError())
        {
            _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
            m_IsQuitting = true;
            Invoke("_DoQuit", 0.5f);
        }
    }

    /// <summary>
    /// 실제 응용프로그램 종료 // 2018-06-18 김재환
    /// Actually quit the application.
    /// </summary>
    private void _DoQuit()
    {
        Application.Quit();
    }

    /// <summary>
    /// 안드로이드 토스트 메시지를 표시한다. // 2018-06-18 김재환
    /// Show an Android toast message.
    /// </summary>
    /// <param name="message">Message string to show in the toast.</param>
    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                    message, 0);
                toastObject.Call("show");
            }));
        }
    }

    /// <summary>
    /// 한번만 선택되도록 rock 거는 용도로 사용됨 // 2018-07-12 김재환
    /// m_IsGnrt 최초값 true
    /// m_IsGnrt = true      생성 가능
    /// m_IsGnrt = false     생성 불가
    /// </summary>
    public void OnClickRockPostion()
    {        
        if (m_IsGnrt)
        {
            m_IsGnrt = false;
            GameObject_rock_postion.GetComponent<Image>().sprite = Resources.Load<Sprite>("Image/icon/Icon_Lock") as Sprite;
            GameObject_rock_scale.SetActive(true);
            GameObject_scalider_zoom.SetActive(true);
            GameObject_Cross.SetActive(false);
        }
        else
        {
            m_IsGnrt = true;
            GameObject_rock_postion.GetComponent<Image>().sprite = Resources.Load<Sprite>("Image/icon/Icon_UnLock") as Sprite;
            GameObject_rock_scale.SetActive(false);
            GameObject_scalider_zoom.SetActive(false);
            GameObject_Cross.SetActive(true);
        }
    }

    /// <summary>
    /// 오브젝트 생성 터치 버튼 // 2018-07-12 김재환
    /// </summary>
    public void OnClickObjectGeneration()
    {
        // 선택한 위치에 한번만 만들어지도록 설정 // 2018-06-19 김재환
        // Set to be created only once at the selected location
        if (m_IsGnrt)
        {
            if (m_GOPiece != null)
            {
                Destroy(m_GOPiece);
                Destroy(m_GOTest);
                m_GOTest = null;
                m_GOPiece = null;
            }
            // 플레이어가 화면을 터치하지 않으면이 업데이트가 완료됩니다.  // 2018-06-18 김재환
            // If the player has not touched the screen, we are done with this update.
            Touch touch;
            touch = Input.GetTouch(0);
            //if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
            //{
            //    return;
            //}

            // 플레이어가 평면을 터치한 위치를 알기위해 Raycast를 보낸다.  // 2018-06-18 김재환
            // Raycast against the location the player touched to search for planes.
            TrackableHit hit;
            TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
                TrackableHitFlags.FeaturePointWithSurfaceNormal;

            if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
            {
                // 플레이어가 선택한 위치와 카메라의 위치를 이용하여 평면의 뒷면인지 확인한다.
                // 만약, 그렇다면 앵커를 만들 필요가 없다. // 2018-06-18 김재환
                // Use hit pose and camera pose to check if hittest is from the
                // back of the plane, if it is, no need to create the anchor.
                if ((hit.Trackable is DetectedPlane) &&
                    Vector3.Dot(FirstPersonCamera.transform.position - hit.Pose.position, hit.Pose.rotation * Vector3.up) < 0)
                {
                    Debug.Log("Hit at back of the current DetectedPlane");
                }
                else
                {
                    ObjectGeneration(hit);
                }
            }
        }
    }

    /// <summary>
    /// 오브젝트 생성 // 2018-07-12 김재환
    /// </summary>
    private void ObjectGeneration(TrackableHit hit)
    {
        // 선택한 위치값을 기억한다.
        // 이값은 오브젝트가 사라졌거나, 다음 조각을 위치하는데 사용되어 진다.
        TH_OriginalLocation = hit;

        // 오브젝트를 리소스에서 가져와서 처리하기
        GameObject prefab1 = Resources.Load("Prefabs/Scenario/" + Singleton.Instance().GameNum + "/Model") as GameObject;

        // 선택한 위치에 Andy 모델을 위치한다. // 2018-06-18 김재환
        // Instantiate Andy model at the hit pose.
        m_GOTest = Instantiate(prefab1, hit.Pose.position, hit.Pose.rotation);

        // raycast에서 멀어지는 방향성에 대해서 보정한다. 및 오브젝트를 회전시켜야 한다. // 2018-06-18 김재환
        // Compensate for the hitPose rotation facing away from the raycast (i.e. camera).
        m_GOTest.transform.Rotate(-90, -180, 0, Space.Self);

        // ARCore에 raycast를 추적할 수 있도록 앵커포인트를 만든다. // 2018-06-18 김재환
        // Create an anchor to allow ARCore to track the hitpoint as understanding of the physical
        // world evolves.
        var anchor = hit.Trackable.CreateAnchor(hit.Pose);

        //// 바닥을 생성해준다.
        //m_GOBottom = Instantiate(BottomPrefab, hit.Pose.position, hit.Pose.rotation);
        //m_GOBottom.transform.Rotate(-90, k_ModelRotation, 0, Space.Self);
        //m_GOBottom.transform.parent = anchor.transform;
        
        // Andy 모델을 앵커포인트의 자식으로 만든다. // 2018-06-18 김재환
        // Make Andy model a child of the anchor.
        m_GOTest.transform.parent = anchor.transform;

        GameObject prefab2 = Resources.Load("Prefabs/Scenario/" + Singleton.Instance().GameNum + "/Piece_1") as GameObject;
        
        // 조각을 다음과 같이 세팅하여 준다.
        m_GOPiece = Instantiate(prefab2, hit.Pose.position, hit.Pose.rotation);
        m_GOPiece.transform.Rotate(0, -180, 0, Space.Self);
        m_GOPiece.transform.parent = anchor.transform;

        Singleton.Instance().Model = m_GOTest;
        Singleton.Instance().PieceParent = m_GOPiece.transform;

        // 트리거 온!
        //충돌체크를 제거해준다.// 테스트용
        m_GOTest.transform.GetComponent<PhysicalControl>().ColliderOnOff("ON", "Object");
        m_GOPiece.transform.GetComponent<PhysicalControl>().ColliderOnOff("ON", "Piece");
    }

    /// <summary>
    /// 다음단계 조각 생성 // 2018-07-25 김재환
    /// </summary>
    public void PieceGeneration(int step)
    {
        if (Resources.Load("Prefabs/Scenario/" + Singleton.Instance().GameNum + "/Piece_" + step.ToString()) as GameObject != null)
        {
            GameObject prefab = Resources.Load("Prefabs/Scenario/" + Singleton.Instance().GameNum + "/Piece_" + step.ToString()) as GameObject;
            m_GOPiece = Instantiate(prefab, TH_OriginalLocation.Pose.position, TH_OriginalLocation.Pose.rotation);
            m_GOPiece.transform.Rotate(0, -180, 0, Space.Self);
            var anchor = TH_OriginalLocation.Trackable.CreateAnchor(TH_OriginalLocation.Pose);
            m_GOPiece.transform.parent = anchor.transform;
            Singleton.Instance().PieceParent = m_GOPiece.transform;
            m_GOPiece.transform.GetComponent<PhysicalControl>().ColliderOnOff("ON", "Piece");
            m_GOPiece.transform.GetComponent<MeshRenderInit>().SetTag();

            m_GOPiece.transform.localScale = new Vector3(1.0f * m_fPieceScale, 1.0f * m_fPieceScale, 1.0f * m_fPieceScale);

            Singleton.Instance().TotalDivision = int.Parse(data[step - 1]["Divisions"].ToString());
        }
    }

    /// <summary>
    /// 슬라이드 조절에 따라 크기 조절 // 2018-07-12 김재환
    /// </summary>
    public void OnChangeScale()
    {
        if (m_GOTest != null)
        {
            m_GOTest.transform.localScale = new Vector3(1.0f * GameObject_scalider_zoom.GetComponent<Slider>().value, 1.0f * GameObject_scalider_zoom.GetComponent<Slider>().value, 1.0f * GameObject_scalider_zoom.GetComponent<Slider>().value);

            m_GOPiece.transform.localScale = new Vector3(1.0f * GameObject_scalider_zoom.GetComponent<Slider>().value, 1.0f * GameObject_scalider_zoom.GetComponent<Slider>().value, 1.0f * GameObject_scalider_zoom.GetComponent<Slider>().value);
        }
    }

    public void OnClickRockScale()
    {
        if (GameObject.Find("DetectedPlaneVisualizer(Clone)") != null)
        {
            GameObject tempGameObject = GameObject.Find("DetectedPlaneVisualizer(Clone)");
            Destroy(tempGameObject);
        }
        

        m_fPieceScale = GameObject_scalider_zoom.GetComponent<Slider>().value;

        m_IsZoom = false;
        GameObject_rock_postion.SetActive(false);
        GameObject_rock_scale.SetActive(false);
        GameObject_scalider_zoom.SetActive(false);

        //GetComponent<Image>().sprite = Resources.Load<Sprite>("Image/icon/Icon_target") as Sprite;
        GameObject_Cross.SetActive(false);
        GameObject_getdrop.SetActive(true);
        Singleton.Instance().GameState = "GameStart";

        for (int iNum = 0; iNum < m_GOPiece.transform.childCount; iNum++)
        {
            m_GOPiece.transform.GetChild(iNum).GetComponent<PieceGetDrop>().PostionMemory();
        }
        GameObject_piece_arrangement.SetActive(true);
    }

    public void OnClickGetDrop()
    {
        if (Singleton.Instance().TouchState == "Ready")
        {
            Singleton.Instance().TouchState = "PieceGet";
        }
        else if (Singleton.Instance().TouchState == "PieceHave")
        {
            Singleton.Instance().TouchState = "PieceDrop";
        }
    }

    public void OnClickRotationLeft()
    {
        if (m_GOTest != null)
        {
            m_GOTest.transform.Rotate(Vector3.forward * 10.0f);
        }
    }

    public void OnClickRotationRight()
    {
        if (m_GOTest != null)
        {
            m_GOTest.transform.Rotate(Vector3.back * 10.0f);
        }        
    }

    public void GameResult()
    {
        Singleton.Instance().GameState = "GameEnd";
        GameObject_Result.SetActive(true);
    }

    public void OnClickScreenShot()
    {
        _ShowAndroidToastMessage("스크린샷이 저장되었습니다.");
    }
    
    public void OnClickShop_Popup()
    {
        GameObject_Shop_popup.SetActive(true);
    }
    
    public void OnClickShop_Ok()
    {
        Application.OpenURL("http://www.fixinc.co.kr/html/00_main/");
        GameObject_Shop_popup.SetActive(false);
    }

    public void OnClickShop_Cancel()
    {
        GameObject_Shop_popup.SetActive(false);
    }

    public void OnClickBack()
    {
        GameObject_Back_popup.SetActive(true);
    }
    
    public void OnClickBack_Ok()
    {
        Singleton.Instance().GameState = "GameReady";
        Singleton.Instance().TouchState = "Null";
        Singleton.Instance().TotalPieces = 24;
        Singleton.Instance().CrashTag = "";
        Singleton.Instance().NowPieces = 0;
        Singleton.Instance().PieceParent = null;
        Singleton.Instance().Model = null;
        
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainSelect");
    }

    public void OnClickBack_Cancel()
    {
        GameObject_Back_popup.SetActive(false);
    }
}
