﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void RigidbodyOnOff(string str)
    {
        if (str == "ON")
        {
            
        }
        else
        {

        }
    }

    public void ColliderOnOff(string s_ONOFF, string s_ObjectPices)
    {        
        if (s_ObjectPices == "Object")
        {
            if (s_ONOFF == "ON")
            {
                for (int iNum = 0; iNum < gameObject.transform.childCount; iNum++)
                {
                    gameObject.transform.GetChild(iNum).GetComponent<SphereCollider>().isTrigger = true;
                }
            }
            else
            {
                for (int iNum = 0; iNum < gameObject.transform.childCount; iNum++)
                {
                    gameObject.transform.GetChild(iNum).GetComponent<SphereCollider>().isTrigger = false;
                }
            }
        }
        else if (s_ObjectPices == "Piece")
        {
            if (s_ONOFF == "ON")
            {
                for (int iNum = 0; iNum < gameObject.transform.childCount; iNum++)
                {
                    gameObject.transform.GetChild(iNum).GetComponent<BoxCollider>().isTrigger = true;
                }
            }
            else
            {
                for (int iNum = 0; iNum < gameObject.transform.childCount; iNum++)
                {
                    gameObject.transform.GetChild(iNum).GetComponent<BoxCollider>().isTrigger = false;
                }
            }
        }
        
    }
}
