﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Crash : MonoBehaviour {

    // 터치한 오브젝트 // 2018-07-06 // 김재환
    private GameObject Piece_obj;
    public GameObject postion_obj;

    public GameObject getdrop_obj;

    // 왼쪽 상단 레벨 게이지
    public GameObject Slider_Level_obj;

    // 한번터치, 두번터치 구분을 위한 값 // 2018-07-06 // 김재환
    private bool B_mToucch;

    // raycast를 위한값; // 2018-07-17 // 김재환
    RaycastHit hitObj;
    Ray ray;
    Vector3 V3_Screen;

    /// <summary>
    /// ray cast를 사용하여 오브젝트를 감별하는데 사용된다. // 2018-07-17 김재환    
    /// </summary>
    public Camera FirstPersonCamera;

    /// <summary>
    /// 선택중인 piece 값 // 2018-07-17 김재환
    /// </summary>
    private string ReadyPieceTag;

    // Use this for initialization
    void Start () {
        Piece_obj = null;
        B_mToucch = false;

        //gameObject.transform.parent = MainCamera.transform;

        V3_Screen = new Vector3(Screen.width / 2, Screen.height / 2, 0);
        ReadyPieceTag = "";
    }

    // Update is called once per frame
    void Update() {

        if (Singleton.Instance().TouchState == "Null" || Singleton.Instance().TouchState == "PieceGet" || Singleton.Instance().TouchState == "Ready")
        {
            ray = FirstPersonCamera.ScreenPointToRay(V3_Screen);
            if (Physics.Raycast(ray, out hitObj, Mathf.Infinity))
            {
                // 부모이름 가져옴
                //Debug.Log("game tag parent : " + hitObj.transform.tag);
                // 자식이름 가져옴
                //Debug.Log("game tag child : " + hitObj.collider.gameObject.tag);
                                
                if (hitObj.collider.gameObject.transform.GetComponent<Characteristic>().Load() != null)
                {
                    // 오브젝트 선택 전 노란색으로 보이는 동작 // 2018-07-17 김재환
                    if (hitObj.collider.gameObject.transform.GetComponent<Characteristic>().Load() == "Piece" && Singleton.Instance().TouchState == "Null")
                    {
                        Singleton.Instance().TouchState = "Ready";
                        hitObj.collider.gameObject.transform.GetComponent<MeshRenderer>().material.color = Color.yellow;
                        ReadyPieceTag = hitObj.collider.gameObject.tag;                       
                    }

                    // 다른오브젝트를 선택했을때, 이번 조각과 비교한 후, 해당 오브젝트를 노란색으로 처리 // 2018-07-17 김재환
                    if (hitObj.collider.gameObject.transform.GetComponent<Characteristic>().Load() == "Piece" && Singleton.Instance().TouchState == "Ready")
                    {
                        if (ReadyPieceTag != hitObj.collider.gameObject.tag)
                        {
                            ReadyPieceTag = hitObj.collider.gameObject.tag;
                            hitObj.transform.GetComponent<MeshRenderInit>().PieceMeshInit();
                            hitObj.collider.gameObject.transform.GetComponent<MeshRenderer>().material.color = Color.yellow;
                        }
                    }

                    // 조각을 선택해서 집었을때 처리 // 2018-07-17 김재환
                    if (Singleton.Instance().GameState == "GameStart" && Singleton.Instance().TouchState == "PieceGet" && hitObj.collider.gameObject.transform.GetComponent<Characteristic>().Load() == "Piece")
                    {
                        ReadyPieceTag = "";
                        Debug.Log("집었다." + hitObj.collider.gameObject.transform.tag);

                        hitObj.collider.gameObject.transform.position = postion_obj.transform.position;
                        hitObj.collider.gameObject.transform.localRotation = Quaternion.Euler(new Vector3(180f, 0f, 0f));

                        Piece_obj = hitObj.collider.gameObject;

                        hitObj.collider.gameObject.transform.SetParent(postion_obj.transform);

                        Singleton.Instance().TouchState = "PieceHave";
                    }
                }
            }
        }

        if (Singleton.Instance().TouchState == "PieceDrop")
        {
            if (Piece_obj != null)
            {
                Piece_obj.transform.GetComponent<PieceGetDrop>().ObjectDrop();
                // Parent_tf.SetParent(Piece_obj.transform);
                //Singleton.Instance().PieceParent.SetParent(Piece_obj.transform);
                Piece_obj.transform.SetParent(Singleton.Instance().PieceParent);
                Piece_obj = null;
                Singleton.Instance().TouchState = "Null";
                getdrop_obj.SetActive(true);
            }
        }

        if (Singleton.Instance().TouchState == "PieceHave")
        {
            getdrop_obj.SetActive(false);
            getdrop_obj.GetComponent<Image>().color = Color.white;
        }

        if (Singleton.Instance().TouchState == "Ready")
        {
            getdrop_obj.SetActive(true);
            getdrop_obj.GetComponent<Image>().color = Color.red;
        }

        if (Singleton.Instance().TouchState == "Null")
        {
            // getdrop_obj.SetActive(true);
            getdrop_obj.GetComponent<Image>().color = Color.white;
        }

        if(Singleton.Instance().CrashTag != "")
        {
            getdrop_obj.SetActive(true);
            Singleton.Instance().CrashTag = "";
            Slider_Level_obj.transform.GetComponent<Slider>().value = Singleton.Instance().NowPieces;

            //if (Singleton.Instance().NowPieces == Singleton.Instance().TotalPieces)
            //{
            //    Singleton.Instance().Model.GetComponent<MeshRenderInit>().Complete();
            //    GameObject.Find("Main Controller").GetComponent<ARController>().GameResult();
            //}
            if (Singleton.Instance().NowPieceStep == Singleton.Instance().TotalPieces)
            {
                Singleton.Instance().Model.GetComponent<MeshRenderInit>().Complete();
                GameObject.Find("Main Controller").GetComponent<ARController>().GameResult();
            }
        }
    }

    public void OnClickRoation()
    {
        if (Piece_obj != null)
        {
            if (Singleton.Instance().TouchState == "PieceHave")
            {
                if (Piece_obj.transform.eulerAngles.z > 350)
                {
                    Piece_obj.transform.eulerAngles = new Vector3(180, 0, 0);
                }

                Piece_obj.transform.eulerAngles = new Vector3(180, 0, Piece_obj.transform.eulerAngles.z + 90);
            }
        }
    }
}