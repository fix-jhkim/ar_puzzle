﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainSelectSceneManager : MonoBehaviour {

    public Image    Image_AR;
    public Text     Text_AR;

    private void Start()
    {
        if (PlayerPrefs.HasKey("AR"))
        {
            if (PlayerPrefs.GetString("AR").Equals("ON"))
            {
                Image_AR.sprite = Resources.Load<Sprite>("Image/Button/Button_AR_on") as Sprite;
                Text_AR.text = "AR 설정/ 켬";
            }
            else
            {
                Image_AR.sprite = Resources.Load<Sprite>("Image/Button/Button_AR_off") as Sprite;
                Text_AR.text = "AR 설정/ 끔";
            }
        }
    }

    public void OnClickGame()
    {
        Singleton.Instance().GameNum = EventSystem.current.currentSelectedGameObject.tag;
        SceneManager.LoadScene("Game");
    }

    public void OnClickBack()
    {
        SceneManager.LoadScene("Main");
    }

    public void OnClickAR()
    {
        if (PlayerPrefs.GetString("AR").Equals("ON"))
        {
            Image_AR.sprite = Resources.Load<Sprite>("Image/Button/Button_AR_off") as Sprite;
            Text_AR.text = "AR 설정/ 끔";
            PlayerPrefs.SetString("AR", "OFF");
        }
        else
        {
            Image_AR.sprite = Resources.Load<Sprite>("Image/Button/Button_AR_on") as Sprite;
            Text_AR.text = "AR 설정/ 켬";
            PlayerPrefs.SetString("AR", "ON");
        }
    }
}