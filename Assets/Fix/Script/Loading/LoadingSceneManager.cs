﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingSceneManager : MonoBehaviour
{
    private WaitForSeconds waitTime;
    private int loadingCount;

    public Text tv_Title;
    public Text tv_button;

    public Image Image_Loading;


    void Start ()
    {
        // 2018.06.29 김재환
        // 엑셀파일 연동
        List<Dictionary<string, object>> data = CSVReader.Read("language");
        
        if (Application.systemLanguage == SystemLanguage.Korean)
        {
            PlayerPrefs.SetString("LANGUAGE", "Korean");
            tv_Title.text = data[1]["kr"].ToString();
            //tv_button.text = data[0]["kr"].ToString();
        }

        //loadingCount = 0;
        waitTime = new WaitForSeconds(0.3f);
        StartCoroutine(LoadScene());
    }
    
    IEnumerator LoadScene()
    {
        while(true)
        {
            loadingCount = loadingCount + 1;
            string str = "Image/loading/Icon_Loading_" + loadingCount.ToString("D2");

            Debug.Log(str);
            Image_Loading.sprite = Resources.Load<Sprite>(str) as Sprite;

            if (loadingCount > 12)
            {
                SceneManager.LoadScene("Main");
            }
            yield return waitTime;
        }
    }

    public void OnSceneClick()
    {
        SceneManager.LoadScene("Main");
    }
}
