﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class Singleton
{
    private static Singleton _instance;
    private static object _synLock = new object();

    // 선택한 게임의 번호를 확인하기 위한 값 // 2018-07-26 김재환
    public string GameNum { get; set; }

    // 게임 상태를 보기 위한 값 // 2018-07-12 김재환
    public string GameState { get; set; }

    // 터치 상태를 보기 위한 값 // 2018-07-13 김재환
    public string TouchState { get; set; }

    // 조각부모를 알아보기 위한 값 // 2018-07-16 김재환
    public Transform PieceParent { get; set; }
    
    // 게임 모델 오브젝트 // 2018-07-16 김재환
    public GameObject Model { get; set; }

    // 내가 선택한 게임 레벨 // 2018-07-16 김재환    
    // 변수명 변경 // 2018-07-24 김재환
    public int TotalPieces { get; set; }

    // 지금 게임 레벨(진행상황) // 2018-07-16 김재환
    // 변수명 변경 // 2018-07-24 김재환
    public int NowPieces { get; set; }

    // 오브젝트 충돌이 일어날경우, 그 상태를 알려줌 // 2018-07-17 김재환
    public string CrashTag { get; set; }

    // 조각 전체 조립 단계 // 2018-07-24 김재환
    public int TotalPieceStep { get; set; }

    // 조각 현재 조립 단계 // 2018-07-24 김재환
    public int NowPieceStep { get; set; }

    // 조각 각 구분 단계 전체 개수 // 2018-07-24 김재환
    public int TotalDivision { get; set; }

    // 조각 각 구분 단계 Step 개수 // 2018-07-24 김재환
    public int NowDivision { get; set; }

    // 생성자를 protected 이상의 한정자로 만듬 // 2018-07-12 김재환
    protected Singleton() { }

    // static 메서도르 생성 해야함 // 2018-07-12 김재환
    public static Singleton Instance()
    {        
        //다중 쓰레드의 경우에는 Instance함수의 동기화가 필요하다. // 2018-07-12 김재환
        if (_instance == null)
            _instance = new Singleton();


        // 다중 쓰레드 환경일 경우 Lock이 필요함 // 2018-07-12 김재환
        if (_instance == null)
        {
            lock (_synLock)
            {
                _instance = new Singleton();
            }
        }
        return _instance;
    }
}