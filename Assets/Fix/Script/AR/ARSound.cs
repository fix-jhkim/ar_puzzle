﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARSound : MonoBehaviour {

    public AudioClip soundExplosion;

    AudioSource myAudio;

    // 정적할당 // 2018-08-07 김재환
    public static ARSound instance;

    private void Awake()
    {
        if (ARSound.instance == null)
            ARSound.instance = this;
    }

    // Use this for initialization
    void Start () {
        myAudio = GetComponent<AudioSource>();
	}

	public void PlaySound()
    {
        myAudio.PlayOneShot(soundExplosion);
    }


	// Update is called once per frame
	void Update () {
		
	}
}
