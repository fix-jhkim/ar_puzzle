﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UI.ProceduralImage;
using UnityEngine.EventSystems;

public class MainSceneManager : MonoBehaviour {

    // 2018-07-11 // 김재환
    // UI를 각 canvas로 컨트롤하기 위한 선언
    public GameObject   Canvas_main;
    public GameObject   Canvas_setting;
    public GameObject   Canvas_language;
    public GameObject   Canvas_account;

    public Image        Image_AR;
    public Image        Image_BGM;
    public Image        Image_SE;

    // 2018-07-11 // 김재환
    // 언어설정 한번 찾은 오브젝트를 계속 사용하기 위함
    private Transform Transform_Language;

    // Use this for initialization
    void Start () {
		
        // 2018-07-11 // 김재환
        // AR 유뮤 확인 후, 처리
        if (PlayerPrefs.HasKey("AR"))
        {
            if (PlayerPrefs.GetString("AR").Equals("ON"))
            {
                Image_AR.sprite = Resources.Load<Sprite>("Image/Button/Button_AR_on") as Sprite;
            }
            else
            {
                Image_AR.sprite = Resources.Load<Sprite>("Image/Button/Button_AR_off") as Sprite;
            }
        }
        else
        {
            PlayerPrefs.SetString("AR", "ON");
            Image_AR.sprite = Resources.Load<Sprite>("Image/Button/Button_AR_on") as Sprite;
        }

        // 2018-07-11 // 김재환
        // 배경음 유무 확인 후, 처리
        if (PlayerPrefs.HasKey("BGM"))
        {
            if (PlayerPrefs.GetString("BGM").Equals("ON"))
            {
                Image_BGM.sprite = Resources.Load<Sprite>("Image/Button/Button_BGM_on") as Sprite;
            }
            else
            {
                Image_BGM.sprite = Resources.Load<Sprite>("Image/Button/Button_BGM_off") as Sprite;
            }
        }
        else
        {
            PlayerPrefs.SetString("BGM", "ON");
            Image_BGM.sprite = Resources.Load<Sprite>("Image/Button/Button_BGM_on") as Sprite;
        }

        // 2018-07-11 // 김재환
        // 사운드 유무 확인 후, 처리
        if (PlayerPrefs.HasKey("SE"))
        {
            if (PlayerPrefs.GetString("SE").Equals("ON"))
            {
                Image_SE.sprite = Resources.Load<Sprite>("Image/Button/Button_SE_on") as Sprite;
            }
            else
            {
                Image_SE.sprite = Resources.Load<Sprite>("Image/Button/Button_SE_off") as Sprite;
            }
        }
        else
        {
            PlayerPrefs.SetString("SE", "ON");
            Image_SE.sprite = Resources.Load<Sprite>("Image/Button/Button_SE_on") as Sprite;
        }        
    }
	
	// Update is called once per frame
	void Update () {
		
    }

    public void OnClickMainSelect()
    {
        SceneManager.LoadScene("MainSelect");
    }

    public void OnClickSetting()
    {
        Canvas_main.SetActive(false);
        Canvas_setting.SetActive(true);        
    }

    public void OnClickSettingBack()
    {
        Canvas_main.SetActive(true);
        Canvas_setting.SetActive(false);        
    }

    public void OnClickLanguage()
    {
        Canvas_language.SetActive(true);
        Canvas_setting.SetActive(false);
        
        // 2018-07-11 // 김재환
        // 언어 설정
        if (PlayerPrefs.HasKey("LANGUAGE"))
        {
            Transform_Language = GameObject.Find(PlayerPrefs.GetString("LANGUAGE")).transform.GetChild(0);
            Transform_Language.GetComponent<ProceduralImage>().color = Color.black;
            Transform_Language.GetComponent<ProceduralImage>().BorderWidth = 0;
        }
        else
        {
            PlayerPrefs.SetString("LANGUAGE", "korean");
        }
    }

    public void OnClickLanguageBack()
    {
        Canvas_language.SetActive(false);
        Canvas_setting.SetActive(true);
    }

    public void OnClickLanguageCountry()
    {
        Transform_Language = GameObject.Find(PlayerPrefs.GetString("LANGUAGE")).transform.GetChild(0);
        Transform_Language.GetComponent<ProceduralImage>().color = Color.white;
        Transform_Language.GetComponent<ProceduralImage>().BorderWidth = 3;

        Transform_Language = GameObject.Find(EventSystem.current.currentSelectedGameObject.gameObject.name).transform.GetChild(0);
        Transform_Language.GetComponent<ProceduralImage>().color = Color.black;
        Transform_Language.GetComponent<ProceduralImage>().BorderWidth = 0;

        PlayerPrefs.SetString("LANGUAGE", EventSystem.current.currentSelectedGameObject.gameObject.name);
    }

    public void OnClickAR()
    {
        if (PlayerPrefs.GetString("AR").Equals("ON"))
        {
            Image_AR.sprite = Resources.Load<Sprite>("Image/Button/Button_AR_off") as Sprite;
            PlayerPrefs.SetString("AR", "OFF");
        }
        else
        {
            Image_AR.sprite = Resources.Load<Sprite>("Image/Button/Button_AR_on") as Sprite;
            PlayerPrefs.SetString("AR", "ON");
        }
    }

    public void OnClickBGM()
    {
        if (PlayerPrefs.GetString("BGM").Equals("ON"))
        {
            Image_BGM.sprite = Resources.Load<Sprite>("Image/Button/Button_BGM_off") as Sprite;
            PlayerPrefs.SetString("BGM", "OFF");
        }
        else
        {
            Image_BGM.sprite = Resources.Load<Sprite>("Image/Button/Button_BGM_on") as Sprite;
            PlayerPrefs.SetString("BGM", "ON");
        }
    }

    public void OnClickSE()
    {
        if (PlayerPrefs.GetString("SE").Equals("ON"))
        {
            Image_SE.sprite = Resources.Load<Sprite>("Image/Button/Button_SE_off") as Sprite;
            PlayerPrefs.SetString("SE", "OFF");
        }
        else
        {
            Image_SE.sprite = Resources.Load<Sprite>("Image/Button/Button_SE_on") as Sprite;
            PlayerPrefs.SetString("SE", "ON");
        }
    }

    public void OnClickAccount()
    {

    }

    public void OnClickBug()
    {

    }

    public void OnClickTutorial()
    {

    }

    public void OnClickTerms()
    {

    }
}
